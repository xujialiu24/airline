package utils;

import constant.Constant;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * Title             : BackGroundImagePanle.java
 * Description : This class is the class that builds the background image.
 */
public class BackGroundImagePanle extends JPanel{
    private ImageIcon icon;

    /*public ImageIcon getIcon() {
        return icon;
    }*/
    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }
    private int x = 0;
    private int y = 0;
    public BackGroundImagePanle(String string) {
        icon = new ImageIcon(Constant.RESOURCE_PATH + string);
    }
    public void paintComponent(Graphics g) {
        g.drawImage(icon.getImage(), x, y, getSize().width,getSize().height, this);// 图片会自动缩放
    }
}
