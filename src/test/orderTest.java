package test;

import org.junit.Test;

import model.order;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
/**
 * Title             : orderTest.java
 * Description : This is a test class.
 */
public class orderTest {
    order model;

    /**
     * setup bufore test
     */
    @Before
    public void setup(){
        model = new order();
    }

    /**
     * a test
     */
    @Test
    public void CheckRightOrder1(){
        assertEquals(1,model.isOrder("1326854870743"));
    }

    /**
     * a test
     */
    @Test
    public void CheckRightOrder2(){
        assertEquals(1,model.isOrder("1326854870748"));
    }

    /**
     * a test
     */
    @Test
    public void CheckRightOrder3(){
        assertEquals(1,model.isOrder("1326854870746"));
    }
    
    /**
     * a test
     */
    @Test
    public void CheckWrongOrder1(){
        assertEquals(0,model.isOrder("13265870746"));
    }

    /**
     * a test
     */
    @Test
    public void CheckWrongOrder2(){
        assertEquals(0,model.isOrder("ascqwdasxqe"));
    }
    
    /**
     * a test
     */
    @Test
    public void CheckWrongOrder3(){
        assertEquals(0,model.isOrder(""));
    }

    /**
     * tear up after test
     */
    @After
    public void tearup(){
        model = null;
    }
}
