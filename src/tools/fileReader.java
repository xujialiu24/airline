package tools;
import java.io.*;

/**
 * Title             : fileReader.java
 * Description : This class is used to read all information in one file
 */
public class fileReader {//Tools for reading files
    /**
     * readFile function is used to read all information in the file you appoint
     * @param file this is the name of the file you want to read
     */
    public static String readFile(String file){
        String content ="";
        try{
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            String line;
            while((line = br.readLine())!=null){
                content += line;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return content;//Return the read text data
    }
}