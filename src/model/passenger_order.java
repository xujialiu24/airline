package model;
import tools.*;

/**
 * Title             : passenger_order.java
 * Description :passenger_ order. Txt file stores the name and order number corresponding to each ID number, and each line represents a unique ID number
 * passenger_order.java can read and edit passenger_order.txt.
 */
public class passenger_order {

    /**
     * findOrder_line function is used to find the line has the order number you want to read
     * @param content this is a order number
     */
    public static String findOrder_line(String content){
        lineReader lr = new lineReader();
        String order_line=lr.readLine("resource/passenger_order.txt",content);
        return order_line;
    }

    /**
     * The findsurname function can view the last name of the owner of the ID number. Adding "ID number" to the parameter in brackets will return "last name"
     * @param content this is an id number
     */
    public static String findSurname(String content) {
        String temp=findOrder_line(content);
        String[] sp =temp.split("\\s+");
        String surname = sp[2];
        return surname;
    }

    /**
     * The findname function can view the name of the owner of the ID card number. Add "order number" to the parameter in brackets, and "name" will be returned
     * @param content this is a order number
     */
    public static String findName(String content) {
        String temp=findOrder_line(content);
        String[] sp =temp.split("\\s+");
        String name = sp[1]+" "+sp[2];
        return name;
    }



    /**
     * The issername method can determine whether the entered last name belongs to the ID number. The parameters in parentheses are the ID number and last name you want to determine. If the ID card matches the last name, an int value of 1 is returned. Otherwise, 0 is returned
     * @param idNum this is a id number
     * @param name this is surname
     */
    public  static int isSurname(String idNum,String name){
        int judgement = 0;
        String surname=findSurname(idNum);
        String[] sp = surname.split("\\s+");
        int i=0;
        while(i<sp.length){
            if(sp[i].equals(name)){
                judgement=1;
                break;
            }
            i=i+1;
        }
        return judgement;
    }


    /**
     * Since there may be multiple flight orders under an ID card, you can use the ordernum function to view the total number of order numbers under the ID card. Add "ID card number" to the parameter in brackets, and an int type number (order quantity) will be returned
     * @param content this is an id number
     */
    public static int orderNum(String content){
        String temp=findOrder_line(content);
        String[] sp = temp.split("\\s+");
        int num = sp.length-3;
        return num;
    }


    /**
     * The findorder function can find all the "order numbers" under the ID card according to the "ID card number". Add "ID card number" and "order serial number (1 represents the first order, 2 represents the second order, and so on)" to the parameters in brackets
     * This function will return the order number you selected (type: String)
     * @param content this is an id number
     */
    public static String findOrder(String content,int num) {
        String temp=findOrder_line(content);
        String[] sp =temp.split("\\s+");
        String order_number = sp[num+2];
        return order_number;
    }


    /**
     * find_ id_ The num function can find the ID card number to which the order number belongs according to the order number, and add the order number to the parameter in brackets
     * This function will return the order number you selected (type: String)
     * @param content this is a order number
     */
    public static String find_id_num(String content) {
        String temp=findOrder_line(content);
        String[] sp =temp.split("\\s+");
        String id_number = sp[0];
        return id_number;
    }

}