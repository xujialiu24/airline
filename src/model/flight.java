package model;
import tools.*;
import java.io.*;
import java.util.*;
/**
 * Title             : flight.java
 * Description :flight. Txt file stores "flight number" corresponding to "order number" and "specific matters (selected food)" (each line represents an order, for example: 1326854870744 ba0570 foodchannel)
 * flight.java can read and edit flight.txt.
 */
public class flight {
    public static String findFlight_line(String content){
        lineReader lr = new lineReader();
        String flight_number_line = lr.readLine("resource/flight.txt",content);
        return flight_number_line;
    }
    /**
     * The findflight function can find the corresponding flight number according to the order number. The parameter (content) in parentheses should be the order number, and the return value (flight_number) is the flight number corresponding to the order number
     * @param content This is a order number
     */
    public static String findFlight(String content){
        String temp = findFlight_line(content);
        String[] sp = temp.split("\\s+");
        String flight_number = sp[1];
        return flight_number;
    }

    /**
     * The findflight function can find the corresponding flight number according to the order number. The parameter (content) in parentheses should be the order number, and the return value (flight_number) is the flight number corresponding to the order number
     * After adding successfully, the unselected "fooddishosen" at the end of the order number of each line will be replaced with the added (string food)
     * @param order_number This is a order number
     * @param food This is the chosen food
     */
    public static void add_food(String order_number,String food){
        lineReader lr = new lineReader();
        lineWriter lw = new lineWriter();
        int lineNum=lr.findLine("resource/flight.txt",order_number);
        String reLine = lw.lineRewrite("resource/flight.txt",lineNum,"FOODUNCHOSEN",food);
        lw.writeLine("resource/flight.txt",lineNum,reLine);
    }

    /**
     * add_ Seat function, add the "order_number" of the item being selected and the "letter" and "number of seat columns" selected in the parameters in brackets     * After adding successfully, the unselected "fooddishosen" at the end of the order number of each line will be replaced with the added (string food)
     * After adding successfully, the unselected "letterunchosen" and "numberunchosen" at the end of each line of order number will be replaced with the added (string letter, string number)
     * @param order_number This is a order number
     * @param letter This is the letter of chosen seat
     * @param number This is the number of chosen seat
     */
    public static void add_seat(String order_number,String letter,String number){
        lineReader lr = new lineReader();
        lineWriter lw = new lineWriter();
        int lineNum=lr.findLine("resource/flight.txt",order_number);
        String reLine1 = lw.lineRewrite("resource/flight.txt",lineNum,"LETTERUNCHOSEN",letter);
        lw.writeLine("resource/flight.txt",lineNum,reLine1);
        String reLine2 = lw.lineRewrite("resource/flight.txt",lineNum,"NUMBERUNCHOSEN",number);
        lw.writeLine("resource/flight.txt",lineNum,reLine2);
    }

    /**
     * add_ note function, it can add note
     * @param order_number This is a order number
     * @param note This is the note
     */
    public static void add_note(String order_number,String note){
        lineReader lr = new lineReader();
        lineWriter lw = new lineWriter();
        int lineNum=lr.findLine("resource/flight.txt",order_number);
        String reLine = lw.lineRewrite("resource/flight.txt",lineNum,"NOTE",note);
        lw.writeLine("resource/flight.txt",lineNum,reLine);
    }


    /**
     * The "order_number" of the item being selected is added to the parameter in parentheses in the boarding function“
     * After adding successfully, the unselected "unboarding" at the end of the order number of each line will be replaced with the added "isboarding"
     * @param order_number This is a order number
     */
    public static void boarding(String order_number){
        lineReader lr = new lineReader();
        lineWriter lw = new lineWriter();
        int lineNum=lr.findLine("resource/flight.txt",order_number);
        String reLine = lw.lineRewrite("resource/flight.txt",lineNum,"UNBOARDING","ISBOARDING");
        lw.writeLine("resource/flight.txt",lineNum,reLine);
    }
    /**
     * isBoarding function is used to know if the passenger has been boarding
     * @param flight_number This is a flight number
     * @param id_number This is an id number
     */
    public static boolean isBoarding(String flight_number,String id_number){
        lineReader lr = new lineReader();
        lineWriter lw = new lineWriter();
        String judgement=null;
        try{
            FileReader fr =new FileReader(new File("resource/flight.txt"));
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while(line!=null){
                if(line.contains(flight_number)){
                    if(line.contains(id_number)){
                        break;
                    }else {
                        line=br.readLine();
                    }
                }else{
                    line=br.readLine();
                }
            }
            judgement = line;
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        String str="ISBOARDING";
        return judgement.contains(str);
    }

    /**
     * find_all_flights function is used to find all lights in box
     */
    public  static  String  find_all_flights(){
        String result=null;
        try{
            FileReader fr =new FileReader(new File("resource/flight.txt"));
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            String[] sp = line.split("\\s+");
            result=sp[1];
            while(line!=null){
                sp=line.split("\\s+");
                result=result+" "+sp[1];
                line = br.readLine();
            }

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return result;
    }

    /**
     * The isflight method can judge whether the entered number is an existing flight number. The parameters in parentheses are the string you want to judge. If there is a flight number, it returns a value of int type 1, otherwise it returns 0
     * @param content This is what user has entered
     */
    public  static int isFlight(String content){
        int judgement=0;
        String[] sp = find_all_flights().split("\\s+");
        int i=0;
        while(i<sp.length){
            if(sp[i].equals(content)){
                judgement=1;
                break;
            }
            i=i+1;
        }
        return judgement;
    }

    /**
     * administer_findEachFlight_Information function is used to find the information of flight according to the flight number
     * @param flight_num This is a flight number
     */
    public static ArrayList<String> administer_findEachFlight_Information(String flight_num){
        ArrayList<String> result=new ArrayList<>();
        try{
            FileReader fr =new FileReader(new File("resource/flight.txt"));
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while(line!=null){
                if(line.contains(flight_num)){
                    result.add(line);
                    line= br.readLine();
                }else{
                    line=br.readLine();
                }
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return result;
    }
    /**
     * administer_findEachFlight_Num function is used to find the flight according to the flight number
     * @param flight_num This is a flight number
     */
    public static int administer_findEachFlight_Num(String flight_num){
        int result=0;
        try{
            FileReader fr =new FileReader(new File("resource/flight.txt"));
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while(line!=null){
                if(line.contains(flight_num)){
                    line= br.readLine();
                    result++;
                }else{
                    line=br.readLine();
                }
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return result;
    }
    /**
     * IS_VIP function is used to know if the passenger is VIP
     * @param order_num This is a order number
     */
    public static int IS_VIP(String order_num){
        int result=0;
        try{
            FileReader fr =new FileReader(new File("resource/flight.txt"));
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            String[] temp =null;
            while(line!=null){
                if(line.contains(order_num)){
                    if(line.contains("VIP")){
                        result=1;
                    }
                    break;
                }else{
                    line = br.readLine();
                }
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return result;
    }
}
