package frame.UserPanel;

import utils.BackGroundImagePanle;
import utils.GBC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 * <p>This page is mainly for reminding the passengers and show them the right way to paste baggage tags.</p>
 *
 * @author Jiayi Yu
 * @version 3.1
 */

public class Video_Panel extends JPanel {
    int index = 1;
    public Video_Panel() {
        // Set background color
        setBackground(new Color(72, 46, 115));

        // Set initial panel
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72, 46, 115));
        panel.setPreferredSize(new Dimension(1200, 680));
        add(panel);

        // Create a new panel and then set size and color of it
        JPanel Tips = new JPanel();
        BorderLayout f1 = new BorderLayout();
        Tips.setLayout(f1);
        Tips.setBounds(0, 0, 1200, 100);
        Tips.setBackground(new Color(72, 46, 115));
        panel.add(Tips);

        // Create a new text label and then set the label
        JLabel title = new JLabel("PLEASE PASTE BAGGAGE TAGS IN THE FOLLOWING WAY", SwingConstants.CENTER);
        title.setFont(new Font(Font.DIALOG, Font.BOLD, 27));//设置文字字体
        title.setForeground(Color.white);//设置文字的颜色
        title.setOpaque(true);
        title.setBackground(new Color(72, 46, 115));
        title.setPreferredSize(new Dimension(800, 80));
        Tips.add(title, BorderLayout.CENTER);

        // Create a panel to contain the image and then set it
        JPanel Rims = new JPanel();
        BorderLayout f2 = new BorderLayout();
        Rims.setLayout(f2);
        Rims.setBounds(0, 0, 900, 600);
        Rims.setBackground(new Color(46, 115, 105));
        panel.add(Rims);
        BackGroundImagePanle R_pic1 = new BackGroundImagePanle("Video_ro.gif");
        R_pic1.setPreferredSize(new Dimension(850, 530));
        Rims.add(R_pic1);

    }

}
