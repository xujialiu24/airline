package frame.UserPanel;

import frame.Main_Frame;
import model.eachFlight;
import utils.BackGroundImagePanle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
/**
 * Title             : Seats_Panel
 * Description : This class build the panel for passenger to choose seat.
 */
public class Seats_Panel extends JPanel {
    public static String letter="0";
    public static String number="0";
     /**
     *  This is the method to reset all buttons unchosen except the seats which have been reserved.
     *  @param jButton1A this is the button of seat1A.
     *  @param jButton1B this is the button of seat1B.
     *  @param jButton1C this is the button of seat1C.
     *  @param jButton1D this is the button of seat1D.
     *  @param jButton1E this is the button of seat1E.
     *  @param jButton1F this is the button of seat1F.
     *  @param jButton2A this is the button of seat2A.
     *  @param jButton2B this is the button of seat2B.
     *  @param jButton2C this is the button of seat2C.
     *  @param jButton2D this is the button of seat2D.
     *  @param jButton2E this is the button of seat2E.
     *  @param jButton2F this is the button of seat2F.
     *  @param jButton3A this is the button of seat3A.
     *  @param jButton3B this is the button of seat3B.
     *  @param jButton3C this is the button of seat3C.
     *  @param jButton3D this is the button of seat3D.
     *  @param jButton3E this is the button of seat3E.
     *  @param jButton3F this is the button of seat3F.
     *  @param jButton4A this is the button of seat4A.
     *  @param jButton4B this is the button of seat4B.
     *  @param jButton4C this is the button of seat4C.
     *  @param jButton4D this is the button of seat4D.
     *  @param jButton4E this is the button of seat4E.
     *  @param jButton4F this is the button of seat4F.
     *  @param jButton5A this is the button of seat5A.
     *  @param jButton5B this is the button of seat5B.
     *  @param jButton5C this is the button of seat5C.
     *  @param jButton5D this is the button of seat5D.
     *  @param jButton5E this is the button of seat5E.
     *  @param jButton5F this is the button of seat5F.
     *  @param jButton6A this is the button of seat6A.
     *  @param jButton6B this is the button of seat6B.
     *  @param jButton6C this is the button of seat6C.
     *  @param jButton6D this is the button of seat6D.
     *  @param jButton6E this is the button of seat6E.
     *  @param jButton6F this is the button of seat6F.
     *  @param jButton7A this is the button of seat7A.
     *  @param jButton7B this is the button of seat7B.
     *  @param jButton7C this is the button of seat7C.
     *  @param jButton7D this is the button of seat7D.
     *  @param jButton7E this is the button of seat7E.
     *  @param jButton7F this is the button of seat7F.
     *  @param jButton8A this is the button of seat8A.
     *  @param jButton8B this is the button of seat8B.
     *  @param jButton8C this is the button of seat8C.
     *  @param jButton8D this is the button of seat8D.
     *  @param jButton8E this is the button of seat8E.
     *  @param jButton8F this is the button of seat8F.
     *  @param jButton9A this is the button of seat9A.
     *  @param jButton9B this is the button of seat9B.
     *  @param jButton9C this is the button of seat9C.
     *  @param jButton9D this is the button of seat9D.
     *  @param jButton9E this is the button of seat9E.
     *  @param jButton9F this is the button of seat9F.
     *  @param jButton10A this is the button of seat10A.
     *  @param jButton10B this is the button of seat10B.
     *  @param jButton10C this is the button of seat10C.
     *  @param jButton10D this is the button of seat10D.
     *  @param jButton10E this is the button of seat10E.
     *  @param jButton10F this is the button of seat10F.
     *  @param jButton11A this is the button of seat11A.
     *  @param jButton11B this is the button of seat11B.
     *  @param jButton11C this is the button of seat11C.
     *  @param jButton11D this is the button of seat11D.
     *  @param jButton11E this is the button of seat11E.
     *  @param jButton11F this is the button of seat11F.
     *  @param jButton12A this is the button of seat12A.
     *  @param jButton12B this is the button of seat12B.
     *  @param jButton12C this is the button of seat12C.
     *  @param jButton12D this is the button of seat12D.
     *  @param jButton12E this is the button of seat12E.
     *  @param jButton12F this is the button of seat12F.
     *  @param jButton14A this is the button of seat14A.
     *  @param jButton14B this is the button of seat14B.
     *  @param jButton14C this is the button of seat14C.
     *  @param jButton14D this is the button of seat14D.
     *  @param jButton14E this is the button of seat14E.
     *  @param jButton14F this is the button of seat14F.
     *  @param jButton15A this is the button of seat15A.
     *  @param jButton15B this is the button of seat15B.
     *  @param jButton15C this is the button of seat15C.
     *  @param jButton15D this is the button of seat15D.
     *  @param jButton15E this is the button of seat15E.
     *  @param jButton15F this is the button of seat15F.
     *  @param jButton16A this is the button of seat16A.
     *  @param jButton16B this is the button of seat16B.
     *  @param jButton16C this is the button of seat16C.
     *  @param jButton16D this is the button of seat16D.
     *  @param jButton16E this is the button of seat16E.
     *  @param jButton16F this is the button of seat16F.
     *  @param jButton17A this is the button of seat17A.
     *  @param jButton17B this is the button of seat17B.
     *  @param jButton17C this is the button of seat17C.
     *  @param jButton17D this is the button of seat17D.
     *  @param jButton17E this is the button of seat17E.
     *  @param jButton17F this is the button of seat17F.
     *  @param jButton18A this is the button of seat18A.
     *  @param jButton18B this is the button of seat18B.
     *  @param jButton18C this is the button of seat18C.
     *  @param jButton18D this is the button of seat18D.
     *  @param jButton18E this is the button of seat18E.
     *  @param jButton18F this is the button of seat18F.
     *  @param jButton19A this is the button of seat19A.
     *  @param jButton19B this is the button of seat19B.
     *  @param jButton19C this is the button of seat19C.
     *  @param jButton19D this is the button of seat19D.
     *  @param jButton19E this is the button of seat19E.
     *  @param jButton19F this is the button of seat19F.
     *  @param jButton20A this is the button of seat20A.
     *  @param jButton20B this is the button of seat20B.
     *  @param jButton20C this is the button of seat20C.
     *  @param jButton20D this is the button of seat20D.
     *  @param jButton20E this is the button of seat20E.
     *  @param jButton20F this is the button of seat20F.
     */
    public static void reset_seatChosen(
            JButton jButton1A,JButton jButton1B,JButton jButton1C,JButton jButton1D,JButton jButton1E,JButton jButton1F
            ,JButton jButton2A,JButton jButton2B,JButton jButton2C,JButton jButton2D,JButton jButton2E,JButton jButton2F
            ,JButton jButton3A,JButton jButton3B,JButton jButton3C,JButton jButton3D,JButton jButton3E,JButton jButton3F
            ,JButton jButton4A,JButton jButton4B,JButton jButton4C,JButton jButton4D,JButton jButton4E,JButton jButton4F
            ,JButton jButton5A,JButton jButton5B,JButton jButton5C,JButton jButton5D,JButton jButton5E,JButton jButton5F
            ,JButton jButton6A,JButton jButton6B,JButton jButton6C,JButton jButton6D,JButton jButton6E,JButton jButton6F
            ,JButton jButton7A,JButton jButton7B,JButton jButton7C,JButton jButton7D,JButton jButton7E,JButton jButton7F
            ,JButton jButton8A,JButton jButton8B,JButton jButton8C,JButton jButton8D,JButton jButton8E,JButton jButton8F
            ,JButton jButton9A,JButton jButton9B,JButton jButton9C,JButton jButton9D,JButton jButton9E,JButton jButton9F
            ,JButton jButton10A,JButton jButton10B,JButton jButton10C,JButton jButton10D,JButton jButton10E,JButton jButton10F
            ,JButton jButton11A,JButton jButton11B,JButton jButton11C,JButton jButton11D,JButton jButton11E,JButton jButton11F
            ,JButton jButton12A,JButton jButton12B,JButton jButton12C,JButton jButton12D,JButton jButton12E,JButton jButton12F
            ,JButton jButton14A,JButton jButton14B,JButton jButton14C,JButton jButton14D,JButton jButton14E,JButton jButton14F
            ,JButton jButton15A,JButton jButton15B,JButton jButton15C,JButton jButton15D,JButton jButton15E,JButton jButton15F
            ,JButton jButton16A,JButton jButton16B,JButton jButton16C,JButton jButton16D,JButton jButton16E,JButton jButton16F
            ,JButton jButton17A,JButton jButton17B,JButton jButton17C,JButton jButton17D,JButton jButton17E,JButton jButton17F
            ,JButton jButton18A,JButton jButton18B,JButton jButton18C,JButton jButton18D,JButton jButton18E,JButton jButton18F
            ,JButton jButton19A,JButton jButton19B,JButton jButton19C,JButton jButton19D,JButton jButton19E,JButton jButton19F
            ,JButton jButton20A,JButton jButton20B,JButton jButton20C,JButton jButton20D,JButton jButton20E,JButton jButton20F){
        if(jButton1A.isEnabled()){
            jButton1A.setBackground(new Color(235, 119, 6));
        }
        if(jButton1B.isEnabled()){
            jButton1B.setBackground(new Color(235, 119, 6));
        }
        if(jButton1C.isEnabled()){
            jButton1C.setBackground(new Color(235, 119, 6));
        }
        if(jButton1D.isEnabled()){
            jButton1D.setBackground(new Color(235, 119, 6));
        }
        if(jButton1E.isEnabled()){
            jButton1E.setBackground(new Color(235, 119, 6));
        }
        if(jButton1F.isEnabled()){
            jButton1F.setBackground(new Color(235, 119, 6));
        }

        if(jButton2A.isEnabled()){
            jButton2A.setBackground(new Color(8, 75, 57));
        }
        if(jButton2B.isEnabled()){
            jButton2B.setBackground(new Color(8, 75, 57));
        }
        if(jButton2C.isEnabled()){
            jButton2C.setBackground(new Color(8, 75, 57));
        }
        if(jButton2D.isEnabled()){
            jButton2D.setBackground(new Color(8, 75, 57));
        }
        if(jButton2E.isEnabled()){
            jButton2E.setBackground(new Color(8, 75, 57));
        }
        if(jButton2F.isEnabled()){
            jButton2F.setBackground(new Color(8, 75, 57));
        }
        if(jButton3A.isEnabled()){
            jButton3A.setBackground(new Color(8, 75, 57));
        }
        if(jButton3B.isEnabled()){
            jButton3B.setBackground(new Color(8, 75, 57));
        }
        if(jButton3C.isEnabled()){
            jButton3C.setBackground(new Color(8, 75, 57));
        }
        if(jButton3D.isEnabled()){
            jButton3D.setBackground(new Color(8, 75, 57));
        }
        if(jButton3E.isEnabled()){
            jButton3E.setBackground(new Color(8, 75, 57));
        }
        if(jButton3F.isEnabled()){
            jButton3F.setBackground(new Color(8, 75, 57));
        }
        if(jButton4A.isEnabled()){
            jButton4A.setBackground(new Color(8, 75, 57));
        }
        if(jButton4B.isEnabled()){
            jButton4B.setBackground(new Color(8, 75, 57));
        }
        if(jButton4C.isEnabled()){
            jButton4C.setBackground(new Color(8, 75, 57));
        }
        if(jButton4D.isEnabled()){
            jButton4D.setBackground(new Color(8, 75, 57));
        }
        if(jButton4E.isEnabled()){
            jButton4E.setBackground(new Color(8, 75, 57));
        }
        if(jButton4F.isEnabled()){
            jButton4F.setBackground(new Color(8, 75, 57));
        }
        if(jButton5A.isEnabled()){
            jButton5A.setBackground(new Color(68, 130, 109));
        }
        if(jButton5B.isEnabled()){
            jButton5B.setBackground(new Color(68, 130, 109));
        }
        if(jButton5C.isEnabled()){
            jButton5C.setBackground(new Color(68, 130, 109));
        }
        if(jButton5D.isEnabled()){
            jButton5D.setBackground(new Color(68, 130, 109));
        }
        if(jButton5E.isEnabled()){
            jButton5E.setBackground(new Color(68, 130, 109));
        }
        if(jButton5F.isEnabled()){
            jButton5F.setBackground(new Color(68, 130, 109));
        }
        if(jButton6A.isEnabled()){
            jButton6A.setBackground(new Color(68, 130, 109));
        }
        if(jButton6B.isEnabled()){
            jButton6B.setBackground(new Color(68, 130, 109));
        }
        if(jButton6C.isEnabled()){
            jButton6C.setBackground(new Color(68, 130, 109));
        }
        if(jButton6D.isEnabled()){
            jButton6D.setBackground(new Color(68, 130, 109));
        }
        if(jButton6E.isEnabled()){
            jButton6E.setBackground(new Color(68, 130, 109));
        }
        if(jButton6F.isEnabled()){
            jButton6F.setBackground(new Color(68, 130, 109));
        }
        if(jButton7A.isEnabled()){
            jButton7A.setBackground(new Color(68, 130, 109));
        }
        if(jButton7B.isEnabled()){
            jButton7B.setBackground(new Color(68, 130, 109));
        }
        if(jButton7C.isEnabled()){
            jButton7C.setBackground(new Color(68, 130, 109));
        }
        if(jButton7D.isEnabled()){
            jButton7D.setBackground(new Color(68, 130, 109));
        }
        if(jButton7E.isEnabled()){
            jButton7E.setBackground(new Color(68, 130, 109));
        }
        if(jButton7F.isEnabled()){
            jButton7F.setBackground(new Color(68, 130, 109));
        }
        if(jButton8A.isEnabled()){
            jButton8A.setBackground(new Color(68, 130, 109));
        }
        if(jButton8B.isEnabled()){
            jButton8B.setBackground(new Color(68, 130, 109));
        }
        if(jButton8C.isEnabled()){
            jButton8C.setBackground(new Color(68, 130, 109));
        }
        if(jButton8D.isEnabled()){
            jButton8D.setBackground(new Color(68, 130, 109));
        }
        if(jButton8E.isEnabled()){
            jButton8E.setBackground(new Color(68, 130, 109));
        }
        if(jButton8F.isEnabled()){
            jButton8F.setBackground(new Color(68, 130, 109));
        }
        if(jButton9A.isEnabled()){
            jButton9A.setBackground(new Color(68, 130, 109));
        }
        if(jButton9B.isEnabled()){
            jButton9B.setBackground(new Color(68, 130, 109));
        }
        if(jButton9C.isEnabled()){
            jButton9C.setBackground(new Color(68, 130, 109));
        }
        if(jButton9D.isEnabled()){
            jButton9D.setBackground(new Color(68, 130, 109));
        }
        if(jButton9E.isEnabled()){
            jButton9E.setBackground(new Color(68, 130, 109));
        }
        if(jButton9F.isEnabled()){
            jButton9F.setBackground(new Color(68, 130, 109));
        }
        if(jButton10A.isEnabled()){
            jButton10A.setBackground(new Color(235, 119, 6));
        }
        if(jButton10B.isEnabled()){
            jButton10B.setBackground(new Color(235, 119, 6));
        }
        if(jButton10C.isEnabled()){
            jButton10C.setBackground(new Color(235, 119, 6));
        }
        if(jButton10D.isEnabled()){
            jButton10D.setBackground(new Color(235, 119, 6));
        }
        if(jButton10E.isEnabled()){
            jButton10E.setBackground(new Color(235, 119, 6));
        }
        if(jButton10F.isEnabled()){
            jButton10F.setBackground(new Color(235, 119, 6));
        }
        if(jButton11A.isEnabled()){
            jButton11A.setBackground(new Color(68, 130, 109));
        }
        if(jButton11B.isEnabled()){
            jButton11B.setBackground(new Color(68, 130, 109));
        }
        if(jButton11C.isEnabled()){
            jButton11C.setBackground(new Color(68, 130, 109));
        }
        if(jButton11D.isEnabled()){
            jButton11D.setBackground(new Color(68, 130, 109));
        }
        if(jButton11E.isEnabled()){
            jButton11E.setBackground(new Color(68, 130, 109));
        }
        if(jButton11F.isEnabled()){
            jButton11F.setBackground(new Color(68, 130, 109));
        }
        if(jButton12A.isEnabled()){
            jButton12A.setBackground(new Color(68, 130, 109));
        }
        if(jButton12B.isEnabled()){
            jButton12B.setBackground(new Color(68, 130, 109));
        }
        if(jButton12C.isEnabled()){
            jButton12C.setBackground(new Color(68, 130, 109));
        }
        if(jButton12D.isEnabled()){
            jButton12D.setBackground(new Color(68, 130, 109));
        }
        if(jButton12E.isEnabled()){
            jButton12E.setBackground(new Color(68, 130, 109));
        }
        if(jButton12F.isEnabled()){
            jButton12F.setBackground(new Color(68, 130, 109));
        }
        if(jButton14A.isEnabled()){
            jButton14A.setBackground(new Color(68, 130, 109));
        }
        if(jButton14B.isEnabled()){
            jButton14B.setBackground(new Color(68, 130, 109));
        }
        if(jButton14C.isEnabled()){
            jButton14C.setBackground(new Color(68, 130, 109));
        }
        if(jButton14D.isEnabled()){
            jButton14D.setBackground(new Color(68, 130, 109));
        }
        if(jButton14E.isEnabled()){
            jButton14E.setBackground(new Color(68, 130, 109));
        }
        if(jButton14F.isEnabled()){
            jButton14F.setBackground(new Color(68, 130, 109));
        }
        if(jButton15A.isEnabled()){
            jButton15A.setBackground(new Color(68, 130, 109));
        }
        if(jButton15B.isEnabled()){
            jButton15B.setBackground(new Color(68, 130, 109));
        }
        if(jButton15C.isEnabled()){
            jButton15C.setBackground(new Color(68, 130, 109));
        }
        if(jButton15D.isEnabled()){
            jButton15D.setBackground(new Color(68, 130, 109));
        }
        if(jButton15E.isEnabled()){
            jButton15E.setBackground(new Color(68, 130, 109));
        }
        if(jButton15F.isEnabled()){
            jButton15F.setBackground(new Color(68, 130, 109));
        }
        if(jButton16A.isEnabled()){
            jButton16A.setBackground(new Color(68, 130, 109));
        }
        if(jButton16B.isEnabled()){
            jButton16B.setBackground(new Color(68, 130, 109));
        }
        if(jButton16C.isEnabled()){
            jButton16C.setBackground(new Color(68, 130, 109));
        }
        if(jButton16D.isEnabled()){
            jButton16D.setBackground(new Color(68, 130, 109));
        }
        if(jButton16E.isEnabled()){
            jButton16E.setBackground(new Color(68, 130, 109));
        }
        if(jButton16F.isEnabled()){
            jButton16F.setBackground(new Color(68, 130, 109));
        }
        if(jButton17A.isEnabled()){
            jButton17A.setBackground(new Color(68, 130, 109));
        }
        if(jButton17B.isEnabled()){
            jButton17B.setBackground(new Color(68, 130, 109));
        }
        if(jButton17C.isEnabled()){
            jButton17C.setBackground(new Color(68, 130, 109));
        }
        if(jButton17D.isEnabled()){
            jButton17D.setBackground(new Color(68, 130, 109));
        }
        if(jButton17E.isEnabled()){
            jButton17E.setBackground(new Color(68, 130, 109));
        }
        if(jButton17F.isEnabled()){
            jButton17F.setBackground(new Color(68, 130, 109));
        }
        if(jButton18A.isEnabled()){
            jButton18A.setBackground(new Color(68, 130, 109));
        }
        if(jButton18B.isEnabled()){
            jButton18B.setBackground(new Color(68, 130, 109));
        }
        if(jButton18C.isEnabled()){
            jButton18C.setBackground(new Color(68, 130, 109));
        }
        if(jButton18D.isEnabled()){
            jButton18D.setBackground(new Color(68, 130, 109));
        }
        if(jButton18E.isEnabled()){
            jButton18E.setBackground(new Color(68, 130, 109));
        }
        if(jButton18F.isEnabled()){
            jButton18F.setBackground(new Color(68, 130, 109));
        }
        if(jButton19A.isEnabled()){
            jButton19A.setBackground(new Color(68, 130, 109));
        }
        if(jButton19B.isEnabled()){
            jButton19B.setBackground(new Color(68, 130, 109));
        }
        if(jButton19C.isEnabled()){
            jButton19C.setBackground(new Color(68, 130, 109));
        }
        if(jButton19D.isEnabled()){
            jButton19D.setBackground(new Color(68, 130, 109));
        }
        if(jButton19E.isEnabled()){
            jButton19E.setBackground(new Color(68, 130, 109));
        }
        if(jButton19F.isEnabled()){
            jButton19F.setBackground(new Color(68, 130, 109));
        }
        if(jButton20A.isEnabled()){
            jButton20A.setBackground(new Color(68, 130, 109));
        }
        if(jButton20B.isEnabled()){
            jButton20B.setBackground(new Color(68, 130, 109));
        }
        if(jButton20C.isEnabled()){
            jButton20C.setBackground(new Color(68, 130, 109));
        }
        if(jButton20D.isEnabled()){
            jButton20D.setBackground(new Color(68, 130, 109));
        }
        if(jButton20E.isEnabled()){
            jButton20E.setBackground(new Color(68, 130, 109));
        }
        if(jButton20F.isEnabled()){
            jButton20F.setBackground(new Color(68, 130, 109));
        }

    }
    public Seats_Panel(){
        letter="0";
        number="0";
        Main_Frame.flightNum=Flights_Panel.flight_num;
        //设置背景颜色
        setBackground(new Color(72,46,115));
        //设置初始面板
        JPanel panel = new JPanel();
        panel.setBackground(new Color(250,250,250));
        panel.setPreferredSize(new Dimension(1200,680));
        add(panel);

        panel.setLayout(null);
        BackGroundImagePanle ingPanel= new BackGroundImagePanle("SEAT.jpg");
        ingPanel.setBounds(100,100,1000,400);

        panel.add(ingPanel);
        ingPanel.setLayout(null);

        JPanel panelTOP = new JPanel();
        panelTOP.setLayout(null);
        panelTOP.setBounds(0,0,1200,85);
        panelTOP.setBackground(new Color(72,46,115));


        JPanel panelBOT = new JPanel();
        panelBOT.setLayout(null);
        panelBOT.setBackground(new Color(250,250,250));
        panelBOT.setBounds(40,520,1100,70);


        JLabel labelFlight = new JLabel();
        JLabel labelCompany = new JLabel();
        JLabel labelBoarding = new JLabel();
        JLabel labelTime = new JLabel();
        JLabel labelGate = new JLabel();
        JLabel labelGateNum = new JLabel();

        labelFlight.setFont(new Font(Font.DIALOG,Font.BOLD,25));//设置文字字体
        labelFlight.setForeground(Color.white);//设置文字的颜色
        labelFlight.setText(Flights_Panel.flight_num);
        labelFlight.setBounds(60,17,100,40);

        labelCompany.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//设置文字字体
        labelCompany.setForeground(Color.white);//设置文字的颜色
        labelCompany.setText(eachFlight.COMPANY(Main_Frame.flightNum));
        labelCompany.setBounds(170,17,300,40);

        labelBoarding.setFont(new Font(Font.DIALOG,Font.BOLD,25));//设置文字字体
        labelBoarding.setForeground(Color.white);//设置文字的颜色
        labelBoarding.setText("BOARDING TIME");
        labelBoarding.setBounds(480,17,300,40);

        labelTime.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//设置文字字体
        labelTime.setForeground(Color.white);//设置文字的颜色
        labelTime.setText(eachFlight.boardingTime(Main_Frame.flightNum));
        labelTime.setBounds(710,17,100,40);

        labelGate.setFont(new Font(Font.DIALOG,Font.BOLD,25));//设置文字字体
        labelGate.setForeground(Color.white);//设置文字的颜色
        labelGate.setText("GATE");
        labelGate.setBounds(1000,17,100,40);

        labelGateNum.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//设置文字字体
        labelGateNum.setForeground(Color.white);//设置文字的颜色
        labelGateNum.setText(eachFlight.GATE(Main_Frame.flightNum));
        labelGateNum.setBounds(1085,17,50,40);



        panel.add(panelTOP);

        panelTOP.add(labelBoarding);
        panelTOP.add(labelCompany);
        panelTOP.add(labelFlight);
        panelTOP.add(labelTime);
        panelTOP.add(labelGate);
        panelTOP.add(labelGateNum);

        panel.add(panelBOT);

        JButton jButton1 = new JButton();
        jButton1.setBackground(new Color(96, 62, 151));
        jButton1.setBounds(1,25,37,37);

        JButton jButton2 = new JButton();
        jButton2.setBackground(new Color(235, 119, 6));
        jButton2.setBounds(205,25,37,37);

        JButton jButton3 = new JButton();
        jButton3.setBackground(new Color(8, 75, 57));
        jButton3.setBounds(515,25,37,37);

        JButton jButton4 = new JButton();
        jButton4.setBackground(new Color(68, 130, 109));
        jButton4.setBounds(765,25,37,37);

        JButton jButton5 = new JButton();
        jButton5.setBackground(new Color(218,65,71));
        jButton5.setBounds(920,25,37,37);


        JLabel jLabel1 = new JLabel("Selected Seat");
        jLabel1.setFont(new Font(Font.DIALOG,Font.PLAIN,23));//设置文字字体
        jLabel1.setForeground(Color.BLACK);//设置文字的颜色
        jLabel1.setBounds(45,25,200,40);

        JLabel jLabel2 = new JLabel("Extra Legroom(109£)");
        jLabel2.setFont(new Font(Font.DIALOG,Font.PLAIN,23));//设置文字字体
        jLabel2.setForeground(Color.BLACK);//设置文字的颜色
        jLabel2.setBounds(248,25,250,40);

        JLabel jLabel3 = new JLabel("Preferred(149£)");
        jLabel3.setFont(new Font(Font.DIALOG,Font.PLAIN,23));//设置文字字体
        jLabel3.setForeground(Color.BLACK);//设置文字的颜色
        jLabel3.setBounds(558,25,200,40);

        JLabel jLabel4 = new JLabel("Standard");
        jLabel4.setFont(new Font(Font.DIALOG,Font.PLAIN,23));//设置文字字体
        jLabel4.setForeground(Color.BLACK);//设置文字的颜色
        jLabel4.setBounds(813,25,200,40);

        JLabel jLabel5 = new JLabel("Unavailable");
        jLabel5.setFont(new Font(Font.DIALOG,Font.PLAIN,23));//设置文字字体
        jLabel5.setForeground(Color.BLACK);//设置文字的颜色
        jLabel5.setBounds(973,25,200,40);

        panelBOT.add(jButton1);
        panelBOT.add(jButton2);
        panelBOT.add(jButton3);
        panelBOT.add(jButton4);
        panelBOT.add(jButton5);

        panelBOT.add(jLabel1);
        panelBOT.add(jLabel2);
        panelBOT.add(jLabel3);
        panelBOT.add(jLabel4);
        panelBOT.add(jLabel5);




        //第一列
        JButton jButton1A = new JButton();
        JButton jButton1B = new JButton();
        JButton jButton1C = new JButton();
        JButton jButton1D = new JButton();
        JButton jButton1E = new JButton();
        JButton jButton1F = new JButton();
        JButton jButton2A = new JButton();
        JButton jButton2B = new JButton();
        JButton jButton2C = new JButton();
        JButton jButton2D = new JButton();
        JButton jButton2E = new JButton();
        JButton jButton2F = new JButton();
        JButton jButton3A = new JButton();
        JButton jButton3B = new JButton();
        JButton jButton3C = new JButton();
        JButton jButton3D = new JButton();
        JButton jButton3E = new JButton();
        JButton jButton3F = new JButton();
        JButton jButton4A = new JButton();
        JButton jButton4B = new JButton();
        JButton jButton4C = new JButton();
        JButton jButton4D = new JButton();
        JButton jButton4E = new JButton();
        JButton jButton4F = new JButton();
        JButton jButton5A = new JButton();
        JButton jButton5B = new JButton();
        JButton jButton5C = new JButton();
        JButton jButton5D = new JButton();
        JButton jButton5E = new JButton();
        JButton jButton5F = new JButton();
        JButton jButton6A = new JButton();
        JButton jButton6B = new JButton();
        JButton jButton6C = new JButton();
        JButton jButton6D = new JButton();
        JButton jButton6E = new JButton();
        JButton jButton6F = new JButton();
        JButton jButton7A = new JButton();
        JButton jButton7B = new JButton();
        JButton jButton7C = new JButton();
        JButton jButton7D = new JButton();
        JButton jButton7E = new JButton();
        JButton jButton7F = new JButton();
        JButton jButton8A = new JButton();
        JButton jButton8B = new JButton();
        JButton jButton8C = new JButton();
        JButton jButton8D = new JButton();
        JButton jButton8E = new JButton();
        JButton jButton8F = new JButton();
        JButton jButton9A = new JButton();
        JButton jButton9B = new JButton();
        JButton jButton9C = new JButton();
        JButton jButton9D = new JButton();
        JButton jButton9E = new JButton();
        JButton jButton9F = new JButton();
        JButton jButton10A = new JButton();
        JButton jButton10B = new JButton();
        JButton jButton10C = new JButton();
        JButton jButton10D = new JButton();
        JButton jButton10E = new JButton();
        JButton jButton10F = new JButton();
        JButton jButton11A = new JButton();
        JButton jButton11B = new JButton();
        JButton jButton11C = new JButton();
        JButton jButton11D = new JButton();
        JButton jButton11E = new JButton();
        JButton jButton11F = new JButton();
        JButton jButton12A = new JButton();
        JButton jButton12B = new JButton();
        JButton jButton12C = new JButton();
        JButton jButton12D = new JButton();
        JButton jButton12E = new JButton();
        JButton jButton12F = new JButton();
        JButton jButton14A = new JButton();
        JButton jButton14B = new JButton();
        JButton jButton14C = new JButton();
        JButton jButton14D = new JButton();
        JButton jButton14E = new JButton();
        JButton jButton14F = new JButton();
        JButton jButton15A = new JButton();
        JButton jButton15B = new JButton();
        JButton jButton15C = new JButton();
        JButton jButton15D = new JButton();
        JButton jButton15E = new JButton();
        JButton jButton15F = new JButton();
        JButton jButton16A = new JButton();
        JButton jButton16B = new JButton();
        JButton jButton16C = new JButton();
        JButton jButton16D = new JButton();
        JButton jButton16E = new JButton();
        JButton jButton16F = new JButton();
        JButton jButton17A = new JButton();
        JButton jButton17B = new JButton();
        JButton jButton17C = new JButton();
        JButton jButton17D = new JButton();
        JButton jButton17E = new JButton();
        JButton jButton17F = new JButton();
        JButton jButton18A = new JButton();
        JButton jButton18B = new JButton();
        JButton jButton18C = new JButton();
        JButton jButton18D = new JButton();
        JButton jButton18E = new JButton();
        JButton jButton18F = new JButton();
        JButton jButton19A = new JButton();
        JButton jButton19B = new JButton();
        JButton jButton19C = new JButton();
        JButton jButton19D = new JButton();
        JButton jButton19E = new JButton();
        JButton jButton19F = new JButton();
        JButton jButton20A = new JButton();
        JButton jButton20B = new JButton();
        JButton jButton20C = new JButton();
        JButton jButton20D = new JButton();
        JButton jButton20E = new JButton();
        JButton jButton20F = new JButton();

        jButton1A.setBounds(26,302,37,37);
        jButton1B.setBounds(26,260,37,37);
        jButton1C.setBounds(26,217,37,37);
        jButton1D.setBounds(26,152,37,37);
        jButton1E.setBounds(26,109,37,37);
        jButton1F.setBounds(26,66,37,37);
        ingPanel.add(jButton1A);
        ingPanel.add(jButton1B);
        ingPanel.add(jButton1C);
        ingPanel.add(jButton1D);
        ingPanel.add(jButton1E);
        ingPanel.add(jButton1F);
        jButton1A.setBackground(new Color(235, 119, 6));
        jButton1B.setBackground(new Color(235, 119, 6));
        jButton1C.setBackground(new Color(235, 119, 6));
        jButton1D.setBackground(new Color(235, 119, 6));
        jButton1E.setBackground(new Color(235, 119, 6));
        jButton1F.setBackground(new Color(235, 119, 6));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","01")==0){
            jButton1A.setEnabled(false);
            jButton1A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","01")==0){
            jButton1B.setEnabled(false);
            jButton1B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","01")==0){
            jButton1C.setEnabled(false);
            jButton1C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","01")==0){
            jButton1D.setEnabled(false);
            jButton1D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","01")==0){
            jButton1E.setEnabled(false);
            jButton1E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","01")==0){
            jButton1F.setEnabled(false);
            jButton1F.setBackground(new Color(218,65,71));
        }
        jButton1A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="01";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton1A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton1B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="01";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton1B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton1C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="01";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton1C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton1D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="01";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton1D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton1E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="01";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton1E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton1F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="01";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton1F.setBackground(new Color(96, 62, 151));

            }
        });




        //第二列

        jButton2A.setBounds(75,302,37,37);
        jButton2B.setBounds(75,260,37,37);
        jButton2C.setBounds(75,217,37,37);
        jButton2D.setBounds(75,152,37,37);
        jButton2E.setBounds(75,109,37,37);
        jButton2F.setBounds(75,66,37,37);
        ingPanel.add(jButton2A);
        ingPanel.add(jButton2B);
        ingPanel.add(jButton2C);
        ingPanel.add(jButton2D);
        ingPanel.add(jButton2E);
        ingPanel.add(jButton2F);
        jButton2A.setBackground(new Color(8, 75, 57));
        jButton2B.setBackground(new Color(8, 75, 57));
        jButton2C.setBackground(new Color(8, 75, 57));
        jButton2D.setBackground(new Color(8, 75, 57));
        jButton2E.setBackground(new Color(8, 75, 57));
        jButton2F.setBackground(new Color(8, 75, 57));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","02")==0){
            jButton2A.setEnabled(false);
            jButton2A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","02")==0){
            jButton2B.setEnabled(false);
            jButton2B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","02")==0){
            jButton2C.setEnabled(false);
            jButton2C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","02")==0){
            jButton2D.setEnabled(false);
            jButton2D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","02")==0){
            jButton2E.setEnabled(false);
            jButton2E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","02")==0){
            jButton2F.setEnabled(false);
            jButton2F.setBackground(new Color(218,65,71));
        }
        jButton2A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="02";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton2A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton2B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="02";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton2B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton2C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="02";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton2C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton2D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="02";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton2D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton2E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="02";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton2E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton2F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="02";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton2F.setBackground(new Color(96, 62, 151));

            }
        });
        //第三列

        jButton3A.setBounds(124,302,37,37);
        jButton3B.setBounds(124,260,37,37);
        jButton3C.setBounds(124,217,37,37);
        jButton3D.setBounds(124,152,37,37);
        jButton3E.setBounds(124,109,37,37);
        jButton3F.setBounds(124,66,37,37);
        ingPanel.add(jButton3A);
        ingPanel.add(jButton3B);
        ingPanel.add(jButton3C);
        ingPanel.add(jButton3D);
        ingPanel.add(jButton3E);
        ingPanel.add(jButton3F);
        jButton3A.setBackground(new Color(8, 75, 57));
        jButton3B.setBackground(new Color(8, 75, 57));
        jButton3C.setBackground(new Color(8, 75, 57));
        jButton3D.setBackground(new Color(8, 75, 57));
        jButton3E.setBackground(new Color(8, 75, 57));
        jButton3F.setBackground(new Color(8, 75, 57));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","03")==0){
            jButton3A.setEnabled(false);
            jButton3A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","03")==0){
            jButton3B.setEnabled(false);
            jButton3B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","03")==0){
            jButton3C.setEnabled(false);
            jButton3C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","03")==0){
            jButton3D.setEnabled(false);
            jButton3D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","03")==0){
            jButton3E.setEnabled(false);
            jButton3E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","03")==0){
            jButton3F.setEnabled(false);
            jButton3F.setBackground(new Color(218,65,71));
        }
        jButton3A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="03";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton3A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton3B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="03";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton3B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton3C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="03";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton3C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton3D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="03";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton3D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton3E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="03";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton3E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton3F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="03";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton3F.setBackground(new Color(96, 62, 151));

            }
        });
        //第四列

        jButton4A.setBounds(173,302,37,37);
        jButton4B.setBounds(173,260,37,37);
        jButton4C.setBounds(173,217,37,37);
        jButton4D.setBounds(173,152,37,37);
        jButton4E.setBounds(173,109,37,37);
        jButton4F.setBounds(173,66,37,37);
        ingPanel.add(jButton4A);
        ingPanel.add(jButton4B);
        ingPanel.add(jButton4C);
        ingPanel.add(jButton4D);
        ingPanel.add(jButton4E);
        ingPanel.add(jButton4F);
        jButton4A.setBackground(new Color(8, 75, 57));
        jButton4B.setBackground(new Color(8, 75, 57));
        jButton4C.setBackground(new Color(8, 75, 57));
        jButton4D.setBackground(new Color(8, 75, 57));
        jButton4E.setBackground(new Color(8, 75, 57));
        jButton4F.setBackground(new Color(8, 75, 57));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","04")==0){
            jButton4A.setEnabled(false);
            jButton4A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","04")==0){
            jButton4B.setEnabled(false);
            jButton4B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","04")==0){
            jButton4C.setEnabled(false);
            jButton4C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","04")==0){
            jButton4D.setEnabled(false);
            jButton4D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","04")==0){
            jButton4E.setEnabled(false);
            jButton4E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","04")==0){
            jButton4F.setEnabled(false);
            jButton4F.setBackground(new Color(218,65,71));
        }
        jButton4A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="04";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton4A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton4B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="04";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton4B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton4C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="04";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton4C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton4D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="04";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton4D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton4E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="04";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton4E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton4F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="04";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton4F.setBackground(new Color(96, 62, 151));

            }
        });

        //第五列

        jButton5A.setBounds(228,302,37,37);
        jButton5B.setBounds(228,260,37,37);
        jButton5C.setBounds(228,217,37,37);
        jButton5D.setBounds(228,152,37,37);
        jButton5E.setBounds(228,109,37,37);
        jButton5F.setBounds(228,66,37,37);
        ingPanel.add(jButton5A);
        ingPanel.add(jButton5B);
        ingPanel.add(jButton5C);
        ingPanel.add(jButton5D);
        ingPanel.add(jButton5E);
        ingPanel.add(jButton5F);
        jButton5A.setBackground(new Color(68, 130, 109));
        jButton5B.setBackground(new Color(68, 130, 109));
        jButton5C.setBackground(new Color(68, 130, 109));
        jButton5D.setBackground(new Color(68, 130, 109));
        jButton5E.setBackground(new Color(68, 130, 109));
        jButton5F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","05")==0){
            jButton5A.setEnabled(false);
            jButton5A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","05")==0){
            jButton5B.setEnabled(false);
            jButton5B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","05")==0){
            jButton5C.setEnabled(false);
            jButton5C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","05")==0){
            jButton5D.setEnabled(false);
            jButton5D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","05")==0){
            jButton5E.setEnabled(false);
            jButton5E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","05")==0){
            jButton5F.setEnabled(false);
            jButton5F.setBackground(new Color(218,65,71));
        }
        jButton5A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="05";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton5A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton5B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="05";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton5B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton5C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="05";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton5C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton5D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="05";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton5D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton5E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="05";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton5E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton5F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="05";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton5F.setBackground(new Color(96, 62, 151));

            }
        });


        //第六列

        jButton6A.setBounds(280,302,37,37);
        jButton6B.setBounds(280,260,37,37);
        jButton6C.setBounds(280,217,37,37);
        jButton6D.setBounds(280,152,37,37);
        jButton6E.setBounds(280,109,37,37);
        jButton6F.setBounds(280,66,37,37);
        ingPanel.add(jButton6A);
        ingPanel.add(jButton6B);
        ingPanel.add(jButton6C);
        ingPanel.add(jButton6D);
        ingPanel.add(jButton6E);
        ingPanel.add(jButton6F);
        jButton6A.setBackground(new Color(68, 130, 109));
        jButton6B.setBackground(new Color(68, 130, 109));
        jButton6C.setBackground(new Color(68, 130, 109));
        jButton6D.setBackground(new Color(68, 130, 109));
        jButton6E.setBackground(new Color(68, 130, 109));
        jButton6F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","06")==0){
            jButton6A.setEnabled(false);
            jButton6A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","06")==0){
            jButton6B.setEnabled(false);
            jButton6B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","06")==0){
            jButton6C.setEnabled(false);
            jButton6C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","06")==0){
            jButton6D.setEnabled(false);
            jButton6D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","06")==0){
            jButton6E.setEnabled(false);
            jButton6E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","06")==0){
            jButton6F.setEnabled(false);
            jButton6F.setBackground(new Color(218,65,71));
        }
        jButton6A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="06";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton6A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton6B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="06";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton6B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton6C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="06";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton6C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton6D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="06";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton6D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton6E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="06";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton6E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton6F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="06";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton6F.setBackground(new Color(96, 62, 151));

            }
        });

        //第七列

        jButton7A.setBounds(326,302,37,37);
        jButton7B.setBounds(326,260,37,37);
        jButton7C.setBounds(326,217,37,37);
        jButton7D.setBounds(326,152,37,37);
        jButton7E.setBounds(326,109,37,37);
        jButton7F.setBounds(326,66,37,37);
        ingPanel.add(jButton7A);
        ingPanel.add(jButton7B);
        ingPanel.add(jButton7C);
        ingPanel.add(jButton7D);
        ingPanel.add(jButton7E);
        ingPanel.add(jButton7F);
        jButton7A.setBackground(new Color(68, 130, 109));
        jButton7B.setBackground(new Color(68, 130, 109));
        jButton7C.setBackground(new Color(68, 130, 109));
        jButton7D.setBackground(new Color(68, 130, 109));
        jButton7E.setBackground(new Color(68, 130, 109));
        jButton7F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","07")==0){
            jButton7A.setEnabled(false);
            jButton7A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","07")==0){
            jButton7B.setEnabled(false);
            jButton7B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","07")==0){
            jButton7C.setEnabled(false);
            jButton7C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","07")==0){
            jButton7D.setEnabled(false);
            jButton7D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","07")==0){
            jButton7E.setEnabled(false);
            jButton7E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","07")==0){
            jButton7F.setEnabled(false);
            jButton7F.setBackground(new Color(218,65,71));
        }
        jButton7A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="07";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton7A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton7B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="07";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton7B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton7C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="07";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton7C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton7D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="07";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton7D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton7E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="07";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton7E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton7F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="07";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton7F.setBackground(new Color(96, 62, 151));

            }
        });

        //第八列

        jButton8A.setBounds(375,302,37,37);
        jButton8B.setBounds(375,260,37,37);
        jButton8C.setBounds(375,217,37,37);
        jButton8D.setBounds(375,152,37,37);
        jButton8E.setBounds(375,109,37,37);
        jButton8F.setBounds(375,66,37,37);
        ingPanel.add(jButton8A);
        ingPanel.add(jButton8B);
        ingPanel.add(jButton8C);
        ingPanel.add(jButton8D);
        ingPanel.add(jButton8E);
        ingPanel.add(jButton8F);
        jButton8A.setBackground(new Color(68, 130, 109));
        jButton8B.setBackground(new Color(68, 130, 109));
        jButton8C.setBackground(new Color(68, 130, 109));
        jButton8D.setBackground(new Color(68, 130, 109));
        jButton8E.setBackground(new Color(68, 130, 109));
        jButton8F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","08")==0){
            jButton8A.setEnabled(false);
            jButton8A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","08")==0){
            jButton8B.setEnabled(false);
            jButton8B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","08")==0){
            jButton8C.setEnabled(false);
            jButton8C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","08")==0){
            jButton8D.setEnabled(false);
            jButton8D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","08")==0){
            jButton8E.setEnabled(false);
            jButton8E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","08")==0){
            jButton8F.setEnabled(false);
            jButton8F.setBackground(new Color(218,65,71));
        }
        jButton8A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="08";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton8A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton8B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="08";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton8B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton8C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="08";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton8C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton8D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="08";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton8D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton8E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="08";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton8E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton8F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="08";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton8F.setBackground(new Color(96, 62, 151));

            }
        });


        //第九列

        jButton9A.setBounds(423,302,37,37);
        jButton9B.setBounds(423,260,37,37);
        jButton9C.setBounds(423,217,37,37);
        jButton9D.setBounds(423,152,37,37);
        jButton9E.setBounds(423,109,37,37);
        jButton9F.setBounds(423,66,37,37);
        ingPanel.add(jButton9A);
        ingPanel.add(jButton9B);
        ingPanel.add(jButton9C);
        ingPanel.add(jButton9D);
        ingPanel.add(jButton9E);
        ingPanel.add(jButton9F);
        jButton9A.setBackground(new Color(68, 130, 109));
        jButton9B.setBackground(new Color(68, 130, 109));
        jButton9C.setBackground(new Color(68, 130, 109));
        jButton9D.setBackground(new Color(68, 130, 109));
        jButton9E.setBackground(new Color(68, 130, 109));
        jButton9F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","09")==0){
            jButton9A.setEnabled(false);
            jButton9A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","09")==0){
            jButton9B.setEnabled(false);
            jButton9B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","09")==0){
            jButton9C.setEnabled(false);
            jButton9C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","09")==0){
            jButton9D.setEnabled(false);
            jButton9D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","09")==0){
            jButton9E.setEnabled(false);
            jButton9E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","09")==0){
            jButton9F.setEnabled(false);
            jButton9F.setBackground(new Color(218,65,71));
        }
        jButton9A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="09";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton9A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton9B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="09";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton9B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton9C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="09";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton9C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton9D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="09";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton9D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton9E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="09";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton9E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton9F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="09";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton9F.setBackground(new Color(96, 62, 151));

            }
        });

        //第十列

        jButton10A.setBounds(505,302,37,37);
        jButton10B.setBounds(505,260,37,37);
        jButton10C.setBounds(505,217,37,37);
        jButton10D.setBounds(505,152,37,37);
        jButton10E.setBounds(505,109,37,37);
        jButton10F.setBounds(505,66,37,37);
        ingPanel.add(jButton10A);
        ingPanel.add(jButton10B);
        ingPanel.add(jButton10C);
        ingPanel.add(jButton10D);
        ingPanel.add(jButton10E);
        ingPanel.add(jButton10F);
        jButton10A.setBackground(new Color(235, 119, 6));
        jButton10B.setBackground(new Color(235, 119, 6));
        jButton10C.setBackground(new Color(235, 119, 6));
        jButton10D.setBackground(new Color(235, 119, 6));
        jButton10E.setBackground(new Color(235, 119, 6));
        jButton10F.setBackground(new Color(235, 119, 6));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","10")==0){
            jButton10A.setEnabled(false);
            jButton10A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","10")==0){
            jButton10B.setEnabled(false);
            jButton10B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","10")==0){
            jButton10C.setEnabled(false);
            jButton10C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","10")==0){
            jButton10D.setEnabled(false);
            jButton10D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","10")==0){
            jButton10E.setEnabled(false);
            jButton10E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","10")==0){
            jButton10F.setEnabled(false);
            jButton10F.setBackground(new Color(218,65,71));
        }
        jButton10A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="10";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton10A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton10B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="10";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton10B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton10C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="10";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton10C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton10D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="10";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton10D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton10E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="10";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton10E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton10F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="10";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton10F.setBackground(new Color(96, 62, 151));

            }
        });

        //第十一列


        jButton11A.setBounds(554,302,37,37);
        jButton11B.setBounds(554,260,37,37);
        jButton11C.setBounds(554,217,37,37);
        jButton11D.setBounds(554,152,37,37);
        jButton11E.setBounds(554,109,37,37);
        jButton11F.setBounds(554,66,37,37);
        ingPanel.add(jButton11A);
        ingPanel.add(jButton11B);
        ingPanel.add(jButton11C);
        ingPanel.add(jButton11D);
        ingPanel.add(jButton11E);
        ingPanel.add(jButton11F);
        jButton11A.setBackground(new Color(68, 130, 109));
        jButton11B.setBackground(new Color(68, 130, 109));
        jButton11C.setBackground(new Color(68, 130, 109));
        jButton11D.setBackground(new Color(68, 130, 109));
        jButton11E.setBackground(new Color(68, 130, 109));
        jButton11F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","11")==0){
            jButton11A.setEnabled(false);
            jButton11A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","11")==0){
            jButton11B.setEnabled(false);
            jButton11B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","11")==0){
            jButton11C.setEnabled(false);
            jButton11C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","11")==0){
            jButton11D.setEnabled(false);
            jButton11D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","11")==0){
            jButton11E.setEnabled(false);
            jButton11E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","11")==0){
            jButton11F.setEnabled(false);
            jButton11F.setBackground(new Color(218,65,71));
        }
        jButton11A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="11";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton11A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton11B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="11";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton11B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton11C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="11";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton11C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton11D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="11";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton11D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton11E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="11";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton11E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton11F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="11";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton11F.setBackground(new Color(96, 62, 151));

            }
        });

        //第十二列


        jButton12A.setBounds(598,302,37,37);
        jButton12B.setBounds(598,260,37,37);
        jButton12C.setBounds(598,217,37,37);
        jButton12D.setBounds(598,152,37,37);
        jButton12E.setBounds(598,109,37,37);
        jButton12F.setBounds(598,66,37,37);
        ingPanel.add(jButton12A);
        ingPanel.add(jButton12B);
        ingPanel.add(jButton12C);
        ingPanel.add(jButton12D);
        ingPanel.add(jButton12E);
        ingPanel.add(jButton12F);
        jButton12A.setBackground(new Color(68, 130, 109));
        jButton12B.setBackground(new Color(68, 130, 109));
        jButton12C.setBackground(new Color(68, 130, 109));
        jButton12D.setBackground(new Color(68, 130, 109));
        jButton12E.setBackground(new Color(68, 130, 109));
        jButton12F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","12")==0){
            jButton12A.setEnabled(false);
            jButton12A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","12")==0){
            jButton12B.setEnabled(false);
            jButton12B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","12")==0){
            jButton12C.setEnabled(false);
            jButton12C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","12")==0){
            jButton12D.setEnabled(false);
            jButton12D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","12")==0){
            jButton12E.setEnabled(false);
            jButton12E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","12")==0){
            jButton12F.setEnabled(false);
            jButton12F.setBackground(new Color(218,65,71));
        }
        jButton12A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="12";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton12A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton12B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="12";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton12B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton12C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="12";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton12C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton12D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="12";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton12D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton12E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="12";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton12E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton12F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="12";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton12F.setBackground(new Color(96, 62, 151));

            }
        });




        //第十四列


        jButton14A.setBounds(647,302,37,37);
        jButton14B.setBounds(647,260,37,37);
        jButton14C.setBounds(647,217,37,37);
        jButton14D.setBounds(647,152,37,37);
        jButton14E.setBounds(647,109,37,37);
        jButton14F.setBounds(647,66,37,37);
        ingPanel.add(jButton14A);
        ingPanel.add(jButton14B);
        ingPanel.add(jButton14C);
        ingPanel.add(jButton14D);
        ingPanel.add(jButton14E);
        ingPanel.add(jButton14F);
        jButton14A.setBackground(new Color(68, 130, 109));
        jButton14B.setBackground(new Color(68, 130, 109));
        jButton14C.setBackground(new Color(68, 130, 109));
        jButton14D.setBackground(new Color(68, 130, 109));
        jButton14E.setBackground(new Color(68, 130, 109));
        jButton14F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","14")==0){
            jButton14A.setEnabled(false);
            jButton14A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","14")==0){
            jButton14B.setEnabled(false);
            jButton14B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","14")==0){
            jButton14C.setEnabled(false);
            jButton14C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","14")==0){
            jButton14D.setEnabled(false);
            jButton14D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","14")==0){
            jButton14E.setEnabled(false);
            jButton14E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","14")==0){
            jButton14F.setEnabled(false);
            jButton14F.setBackground(new Color(218,65,71));
        }
        jButton14A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="14";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton14A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton14B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="14";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton14B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton14C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="14";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton14C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton14D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="14";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton14D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton14E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="14";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton14E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton14F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="14";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton14F.setBackground(new Color(96, 62, 151));

            }
        });

        //第十五列


        jButton15A.setBounds(706,302,37,37);
        jButton15B.setBounds(706,260,37,37);
        jButton15C.setBounds(706,217,37,37);
        jButton15D.setBounds(706,152,37,37);
        jButton15E.setBounds(706,109,37,37);
        jButton15F.setBounds(706,66,37,37);
        ingPanel.add(jButton15A);
        ingPanel.add(jButton15B);
        ingPanel.add(jButton15C);
        ingPanel.add(jButton15D);
        ingPanel.add(jButton15E);
        ingPanel.add(jButton15F);
        jButton15A.setBackground(new Color(68, 130, 109));
        jButton15B.setBackground(new Color(68, 130, 109));
        jButton15C.setBackground(new Color(68, 130, 109));
        jButton15D.setBackground(new Color(68, 130, 109));
        jButton15E.setBackground(new Color(68, 130, 109));
        jButton15F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","15")==0){
            jButton15A.setEnabled(false);
            jButton15A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","15")==0){
            jButton15B.setEnabled(false);
            jButton15B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","15")==0){
            jButton15C.setEnabled(false);
            jButton15C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","15")==0){
            jButton15D.setEnabled(false);
            jButton15D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","15")==0){
            jButton15E.setEnabled(false);
            jButton15E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","15")==0){
            jButton15F.setEnabled(false);
            jButton15F.setBackground(new Color(218,65,71));
        }
        jButton15A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="15";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton15A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton15B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="15";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton15B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton15C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="15";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton15C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton15D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="15";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton15D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton15E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="15";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton15E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton15F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="15";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton15F.setBackground(new Color(96, 62, 151));

            }
        });

        //第十六列


        jButton16A.setBounds(753,302,37,37);
        jButton16B.setBounds(753,260,37,37);
        jButton16C.setBounds(753,217,37,37);
        jButton16D.setBounds(753,152,37,37);
        jButton16E.setBounds(753,109,37,37);
        jButton16F.setBounds(753,66,37,37);
        ingPanel.add(jButton16A);
        ingPanel.add(jButton16B);
        ingPanel.add(jButton16C);
        ingPanel.add(jButton16D);
        ingPanel.add(jButton16E);
        ingPanel.add(jButton16F);
        jButton16A.setBackground(new Color(68, 130, 109));
        jButton16B.setBackground(new Color(68, 130, 109));
        jButton16C.setBackground(new Color(68, 130, 109));
        jButton16D.setBackground(new Color(68, 130, 109));
        jButton16E.setBackground(new Color(68, 130, 109));
        jButton16F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","16")==0){
            jButton16A.setEnabled(false);
            jButton16A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","16")==0){
            jButton16B.setEnabled(false);
            jButton16B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","16")==0){
            jButton16C.setEnabled(false);
            jButton16C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","16")==0){
            jButton16D.setEnabled(false);
            jButton16D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","16")==0){
            jButton16E.setEnabled(false);
            jButton16E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","16")==0){
            jButton16F.setEnabled(false);
            jButton16F.setBackground(new Color(218,65,71));
        }
        jButton16A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="16";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton16A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton16B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="16";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton16B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton16C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="16";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton16C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton16D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="16";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton16D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton16E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="16";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton16E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton16F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="16";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton16F.setBackground(new Color(96, 62, 151));

            }
        });

        //第十七列


        jButton17A.setBounds(801,302,37,37);
        jButton17B.setBounds(801,260,37,37);
        jButton17C.setBounds(801,217,37,37);
        jButton17D.setBounds(801,152,37,37);
        jButton17E.setBounds(801,109,37,37);
        jButton17F.setBounds(801,66,37,37);
        ingPanel.add(jButton17A);
        ingPanel.add(jButton17B);
        ingPanel.add(jButton17C);
        ingPanel.add(jButton17D);
        ingPanel.add(jButton17E);
        ingPanel.add(jButton17F);
        jButton17A.setBackground(new Color(68, 130, 109));
        jButton17B.setBackground(new Color(68, 130, 109));
        jButton17C.setBackground(new Color(68, 130, 109));
        jButton17D.setBackground(new Color(68, 130, 109));
        jButton17E.setBackground(new Color(68, 130, 109));
        jButton17F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","17")==0){
            jButton17A.setEnabled(false);
            jButton17A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","17")==0){
            jButton17B.setEnabled(false);
            jButton17B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","17")==0){
            jButton17C.setEnabled(false);
            jButton17C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","17")==0){
            jButton17D.setEnabled(false);
            jButton17D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","17")==0){
            jButton17E.setEnabled(false);
            jButton17E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","17")==0){
            jButton17F.setEnabled(false);
            jButton17F.setBackground(new Color(218,65,71));
        }
        jButton17A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="17";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton17A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton17B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="17";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton17B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton17C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="17";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton17C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton17D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="17";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton17D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton17E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="17";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton17E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton17F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="17";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton17F.setBackground(new Color(96, 62, 151));

            }
        });

        //第十八列


        jButton18A.setBounds(850,302,37,37);
        jButton18B.setBounds(850,260,37,37);
        jButton18C.setBounds(850,217,37,37);
        jButton18D.setBounds(850,152,37,37);
        jButton18E.setBounds(850,109,37,37);
        jButton18F.setBounds(850,66,37,37);
        ingPanel.add(jButton18A);
        ingPanel.add(jButton18B);
        ingPanel.add(jButton18C);
        ingPanel.add(jButton18D);
        ingPanel.add(jButton18E);
        ingPanel.add(jButton18F);
        jButton18A.setBackground(new Color(68, 130, 109));
        jButton18B.setBackground(new Color(68, 130, 109));
        jButton18C.setBackground(new Color(68, 130, 109));
        jButton18D.setBackground(new Color(68, 130, 109));
        jButton18E.setBackground(new Color(68, 130, 109));
        jButton18F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","18")==0){
            jButton18A.setEnabled(false);
            jButton18A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","18")==0){
            jButton18B.setEnabled(false);
            jButton18B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","18")==0){
            jButton18C.setEnabled(false);
            jButton18C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","18")==0){
            jButton18D.setEnabled(false);
            jButton18D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","18")==0){
            jButton18E.setEnabled(false);
            jButton18E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","18")==0){
            jButton18F.setEnabled(false);
            jButton18F.setBackground(new Color(218,65,71));
        }
        jButton18A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="18";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton18A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton18B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="18";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton18B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton18C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="18";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton18C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton18D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="18";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton18D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton18E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="18";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton18E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton18F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="18";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton18F.setBackground(new Color(96, 62, 151));

            }
        });

        //第十九列


        jButton19A.setBounds(907,302,37,37);
        jButton19B.setBounds(907,260,37,37);
        jButton19C.setBounds(907,217,37,37);
        jButton19D.setBounds(907,152,37,37);
        jButton19E.setBounds(907,109,37,37);
        jButton19F.setBounds(907,66,37,37);
        ingPanel.add(jButton19A);
        ingPanel.add(jButton19B);
        ingPanel.add(jButton19C);
        ingPanel.add(jButton19D);
        ingPanel.add(jButton19E);
        ingPanel.add(jButton19F);
        jButton19A.setBackground(new Color(68, 130, 109));
        jButton19B.setBackground(new Color(68, 130, 109));
        jButton19C.setBackground(new Color(68, 130, 109));
        jButton19D.setBackground(new Color(68, 130, 109));
        jButton19E.setBackground(new Color(68, 130, 109));
        jButton19F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","19")==0){
            jButton19A.setEnabled(false);
            jButton19A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","19")==0){
            jButton19B.setEnabled(false);
            jButton19B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","19")==0){
            jButton19C.setEnabled(false);
            jButton19C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","19")==0){
            jButton19D.setEnabled(false);
            jButton19D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","19")==0){
            jButton19E.setEnabled(false);
            jButton19E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","19")==0){
            jButton19F.setEnabled(false);
            jButton19F.setBackground(new Color(218,65,71));
        }
        jButton19A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="19";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton19A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton19B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="19";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton19B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton19C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="19";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton19C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton19D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="19";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton19D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton19E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="19";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton19E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton19F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="19";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton19F.setBackground(new Color(96, 62, 151));

            }
        });

        //第二十列


        jButton20A.setBounds(954,302,37,37);
        jButton20B.setBounds(954,260,37,37);
        jButton20C.setBounds(954,217,37,37);
        jButton20D.setBounds(954,152,37,37);
        jButton20E.setBounds(954,109,37,37);
        jButton20F.setBounds(954,66,37,37);
        ingPanel.add(jButton20A);
        ingPanel.add(jButton20B);
        ingPanel.add(jButton20C);
        ingPanel.add(jButton20D);
        ingPanel.add(jButton20E);
        ingPanel.add(jButton20F);
        jButton20A.setBackground(new Color(68, 130, 109));
        jButton20B.setBackground(new Color(68, 130, 109));
        jButton20C.setBackground(new Color(68, 130, 109));
        jButton20D.setBackground(new Color(68, 130, 109));
        jButton20E.setBackground(new Color(68, 130, 109));
        jButton20F.setBackground(new Color(68, 130, 109));
        if (eachFlight.seat(Main_Frame.flightNum,"seatA","20")==0){
            jButton20A.setEnabled(false);
            jButton20A.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatB","20")==0){
            jButton20B.setEnabled(false);
            jButton20B.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatC","20")==0){
            jButton20C.setEnabled(false);
            jButton20C.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatD","20")==0){
            jButton20D.setEnabled(false);
            jButton20D.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatE","20")==0){
            jButton20E.setEnabled(false);
            jButton20E.setBackground(new Color(218,65,71));
        }
        if (eachFlight.seat(Main_Frame.flightNum,"seatF","20")==0){
            jButton20F.setEnabled(false);
            jButton20F.setBackground(new Color(218,65,71));
        }
        jButton20A.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatA";
                number="20";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton20A.setBackground(new Color(96, 62, 151));
            }
        });

        jButton20B.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatB";
                number="20";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton20B.setBackground(new Color(96, 62, 151));

            }
        });

        jButton20C.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatC";
                number="20";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton20C.setBackground(new Color(96, 62, 151));


            }
        });

        jButton20D.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatD";
                number="20";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton20D.setBackground(new Color(96, 62, 151));

            }
        });

        jButton20E.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatE";
                number="20";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton20E.setBackground(new Color(96, 62, 151));

            }
        });

        jButton20F.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                letter="seatF";
                number="20";
                reset_seatChosen(jButton1A,jButton1B,jButton1C,jButton1D,jButton1E,jButton1F
                        ,jButton2A,jButton2B,jButton2C,jButton2D,jButton2E,jButton2F
                        ,jButton3A,jButton3B, jButton3C, jButton3D, jButton3E, jButton3F
                        ,jButton4A,jButton4B, jButton4C, jButton4D, jButton4E, jButton4F
                        ,jButton5A,jButton5B, jButton5C, jButton5D, jButton5E, jButton5F
                        ,jButton6A,jButton6B, jButton6C, jButton6D, jButton6E, jButton6F
                        ,jButton7A,jButton7B, jButton7C, jButton7D, jButton7E, jButton7F
                        ,jButton8A,jButton8B, jButton8C, jButton8D, jButton8E, jButton8F
                        ,jButton9A,jButton9B, jButton9C, jButton9D, jButton9E, jButton9F
                        ,jButton10A,jButton10B, jButton10C, jButton10D, jButton10E, jButton10F
                        ,jButton11A,jButton11B, jButton11C, jButton11D, jButton11E, jButton11F
                        ,jButton12A, jButton12B, jButton12C, jButton12D, jButton12E, jButton12F
                        ,jButton14A, jButton14B, jButton14C, jButton14D, jButton14E, jButton14F
                        ,jButton15A, jButton15B, jButton15C, jButton15D, jButton15E, jButton15F
                        ,jButton16A, jButton16B, jButton16C, jButton16D, jButton16E, jButton16F
                        ,jButton17A, jButton17B, jButton17C, jButton17D, jButton17E, jButton17F
                        ,jButton18A, jButton18B, jButton18C, jButton18D, jButton18E, jButton18F
                        ,jButton19A, jButton19B, jButton19C, jButton19D, jButton19E, jButton19F
                        ,jButton20A, jButton20B, jButton20C, jButton20D, jButton20E, jButton20F);
                jButton20F.setBackground(new Color(96, 62, 151));

            }
        });
    }
}
