package frame.UserPanel;

import model.flight;

import javax.swing.*;
import java.awt.*;

/**
 * This page is for displaying print payment GUI.
 *
 * @author Wenlingxiao Li
 */
public class Pay_Panel extends JPanel {

    //food and seat
    private String food;
    private String seat;
    //price of food and seat
    private float foodPrice;
    private float seatPrice;


    public Pay_Panel(){
        //according to arguments,choose to displayed different label
        if (flight.IS_VIP(Flights_Panel.flight_order)==1){

            if(Foods_Panel.food.equals("standard")){
                this.food = "STANDARD";
                this.foodPrice = 0.0F;
            }
            else if(Foods_Panel.food.equals("vegetarian")){
                this.food = "VEGETARIAN";
                this.foodPrice = 0.0F;
            }
            else if(Foods_Panel.food.equals("halal")){
                this.food = "HALAL";
                this.foodPrice = 0.0F;
            }
            else if(Foods_Panel.food.equals("seafood")){
                this.food = "SEAFOOD";
                this.foodPrice = 12.99F;
            }
            else if(Foods_Panel.food.equals("steak")){
                this.food = "STEAK";
                this.foodPrice = 19.99F;
            }
            else if(Foods_Panel.food.equals("sushi")){
                this.food = "SUSHI";
                this.foodPrice = 15.99F;
            }

            if(Seats_Panel.number.equals("01")){
                this.seat = "EXTRA SEAT";
                this.seatPrice = 119.50F;
            }
            else if(Seats_Panel.number.equals("02") || Seats_Panel.number.equals("03") ||
                    Seats_Panel.number.equals("04")){
                this.seat = "PREFERRED SEAT";
                this.seatPrice = 87.50F;
            }
            else if(Seats_Panel.number.equals("10")){
                this.seat = "EXTRA SEAT";
                this.seatPrice = 119.50F;
            }
            else{
                this.seat = "STANDARD SEAT";
                this.seatPrice = 0.00F;
            }

            //background color setting
            setBackground(new Color(72,46,115));
            //initializing panel
            JPanel panel = new JPanel();
            panel.setBackground(new Color(72,46,115));
            panel.setPreferredSize(new Dimension(1200,680));
            add(panel);
            panel.setLayout(null);

            //initializing sub-panel and adding color
            JPanel backGround = new JPanel();
            backGround.setBackground(new Color(96,62,151));
            backGround.setBounds(75,150,1050,500);
            backGround.setLayout(null);
            panel.add(backGround);

            //adding labels and buttons
            JLabel Hint1 = new JLabel("YOU ARE A VIP USER AND THE FOLLOWING PRICES ARE 20% OFF");
            Hint1.setBackground(new Color(96,62,151));
            Hint1.setBounds(0,20,1200,75);
            Hint1.setBackground(new Color(96,62,151));
            Hint1.setForeground(Color.white);
            Hint1.setFont(new Font (Font.DIALOG, Font.BOLD, 30));
            Hint1.setHorizontalAlignment(JTextField.CENTER);
            panel.add(Hint1);

            JLabel Hint2 = new JLabel("PLEASE CONFIRM AND SCAN QRCODE FOR PAYMENT");
            Hint2.setBackground(new Color(96,62,151));
            Hint2.setBounds(0,65,1200,75);
            Hint2.setBackground(new Color(96,62,151));
            Hint2.setForeground(Color.white);
            Hint2.setFont(new Font (Font.DIALOG, Font.BOLD, 30));
            Hint2.setHorizontalAlignment(JTextField.CENTER);
            panel.add(Hint2);

            JLabel Hint3 = new JLabel("This is your extra service");
            Hint3.setBackground(new Color(72,46,115));
            Hint3.setBounds(80,0,670,140);
            Hint3.setForeground(Color.white);
            Hint3.setFont(new Font (Font.DIALOG, Font.BOLD, 50));
            Hint3.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Hint3);

            JLabel Hint4 = new JLabel(food);//food label
            Hint4.setBackground(new Color(72,46,115));
            Hint4.setBounds(80,140,500,100);
            Hint4.setForeground(Color.white);
            Hint4.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Hint4.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Hint4);

            JLabel Hint5 = new JLabel(seat);//seat label
            Hint5.setBackground(new Color(72,46,115));
            Hint5.setBounds(80,240,500,100);
            Hint5.setForeground(Color.white);
            Hint5.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Hint5.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Hint5);

            JLabel Hint6 = new JLabel("TOTAL");//"total" label
            Hint6.setBackground(new Color(72,46,115));
            Hint6.setBounds(80,340,500,100);
            Hint6.setForeground(Color.white);
            Hint6.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Hint6.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Hint6);

            JLabel Price4 = new JLabel("£" + foodPrice);//foodPrice label
            Price4.setBackground(new Color(72,46,115));
            Price4.setBounds(540,140,170,100);
            Price4.setForeground(Color.white);
            Price4.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Price4.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Price4);

            JLabel Price5 = new JLabel("£" + seatPrice);//seatPrice label
            Price5.setBackground(new Color(72,46,115));
            Price5.setBounds(540,240,170,100);
            Price5.setForeground(Color.white);
            Price5.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Price5.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Price5);

            JLabel Price6 = new JLabel("£" + (foodPrice + seatPrice));//totalPrice label
            Price6.setBackground(new Color(72,46,115));
            Price6.setBounds(540,340,170,100);
            Price6.setForeground(Color.white);
            Price6.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Price6.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Price6);

            JLabel QRcodePlain = new JLabel(new ImageIcon("resource/QRcode.png"));//adding QR code
            QRcodePlain.setBounds(710,0,300,500);
            backGround.add(QRcodePlain);
        }else {
            //according to arguments,choose to displayed different label
            if(Foods_Panel.food.equals("standard")){
                this.food = "STANDARD";
                this.foodPrice = 0.0F;
            }
            else if(Foods_Panel.food.equals("vegetarian")){
                this.food = "VEGETARIAN";
                this.foodPrice = 0.0F;
            }
            else if(Foods_Panel.food.equals("halal")){
                this.food = "HALAL";
                this.foodPrice = 0.0F;
            }
            else if(Foods_Panel.food.equals("seafood")){
                this.food = "SEAFOOD";
                this.foodPrice = 15.99F;
            }
            else if(Foods_Panel.food.equals("steak")){
                this.food = "STEAK";
                this.foodPrice = 24.99F;
            }
            else if(Foods_Panel.food.equals("sushi")){
                this.food = "SUSHI";
                this.foodPrice = 19.99F;
            }

            if(Seats_Panel.number.equals("01")){
                this.seat = "EXTRA SEAT";
                this.seatPrice = 149.50F;
            }
            else if(Seats_Panel.number.equals("02") || Seats_Panel.number.equals("03") ||
                    Seats_Panel.number.equals("04")){
                this.seat = "PREFERRED SEAT";
                this.seatPrice = 109.00F;
            }
            else if(Seats_Panel.number.equals("10")){
                this.seat = "EXTRA SEAT";
                this.seatPrice = 149.00F;
            }
            else{
                this.seat = "STANDARD SEAT";
                this.seatPrice = 0.00F;
            }

            //background color seeting
            setBackground(new Color(72,46,115));
            //iniatializing panel
            JPanel panel = new JPanel();
            panel.setBackground(new Color(72,46,115));
            panel.setPreferredSize(new Dimension(1200,680));
            add(panel);
            panel.setLayout(null);

            //adding sub-panel, adding bgc
            JPanel backGround = new JPanel();
            backGround.setBackground(new Color(96,62,151));
            backGround.setBounds(75,150,1050,500);
            backGround.setLayout(null);
            panel.add(backGround);

            //adding labels and buttons
            JLabel Hint1 = new JLabel("YOU HAVE SELECTED THE FOLLOWING ADDTIONAL PAYMENT SERVICES");
            Hint1.setBackground(new Color(96,62,151));
            Hint1.setBounds(0,20,1200,75);
            Hint1.setBackground(new Color(96,62,151));
            Hint1.setForeground(Color.white);
            Hint1.setFont(new Font (Font.DIALOG, Font.BOLD, 30));
            Hint1.setHorizontalAlignment(JTextField.CENTER);
            panel.add(Hint1);

            JLabel Hint2 = new JLabel("PLEASE CONFIRM AND SCAN QRCODE FOR PAYMENT");
            Hint2.setBackground(new Color(96,62,151));
            Hint2.setBounds(0,65,1200,75);
            Hint2.setBackground(new Color(96,62,151));
            Hint2.setForeground(Color.white);
            Hint2.setFont(new Font (Font.DIALOG, Font.BOLD, 30));
            Hint2.setHorizontalAlignment(JTextField.CENTER);
            panel.add(Hint2);

            JLabel Hint3 = new JLabel("This is your extra service");
            Hint3.setBackground(new Color(72,46,115));
            Hint3.setBounds(80,0,670,140);
            Hint3.setForeground(Color.white);
            Hint3.setFont(new Font (Font.DIALOG, Font.BOLD, 50));
            Hint3.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Hint3);

            JLabel Hint4 = new JLabel(food);//食物标签
            Hint4.setBackground(new Color(72,46,115));
            Hint4.setBounds(80,140,500,100);
            Hint4.setForeground(Color.white);
            Hint4.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Hint4.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Hint4);

            JLabel Hint5 = new JLabel(seat);//座位标签
            Hint5.setBackground(new Color(72,46,115));
            Hint5.setBounds(80,240,500,100);
            Hint5.setForeground(Color.white);
            Hint5.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Hint5.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Hint5);

            JLabel Hint6 = new JLabel("TOTAL");//总计标签
            Hint6.setBackground(new Color(72,46,115));
            Hint6.setBounds(80,340,500,100);
            Hint6.setForeground(Color.white);
            Hint6.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Hint6.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Hint6);

            JLabel Price4 = new JLabel("£" + foodPrice);//食物价格标签
            Price4.setBackground(new Color(72,46,115));
            Price4.setBounds(540,140,170,100);
            Price4.setForeground(Color.white);
            Price4.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Price4.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Price4);

            JLabel Price5 = new JLabel("£" + seatPrice);//座位价格标签
            Price5.setBackground(new Color(72,46,115));
            Price5.setBounds(540,240,170,100);
            Price5.setForeground(Color.white);
            Price5.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Price5.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Price5);

            JLabel Price6 = new JLabel("£" + (foodPrice + seatPrice));//总计价格标签
            Price6.setBackground(new Color(72,46,115));
            Price6.setBounds(540,340,170,100);
            Price6.setForeground(Color.white);
            Price6.setFont(new Font (Font.DIALOG, Font.BOLD, 37));
            Price6.setHorizontalAlignment(JTextField.LEFT);
            backGround.add(Price6);

            JLabel QRcodePlain = new JLabel(new ImageIcon("resource/QRcode.png"));//二维码
            QRcodePlain.setBounds(710,0,300,500);
            backGround.add(QRcodePlain);
        }
    }
}
