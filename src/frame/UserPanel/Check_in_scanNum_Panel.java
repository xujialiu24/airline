package frame.UserPanel;

import constant.Constant;

import javax.swing.*;
import java.awt.*;

/**
 * <p>This page is mainly used to log in passengers by scanning their ID cards.</p>
 *
 * @author Qingwei Gao
 * @version 3.1
 */
public class Check_in_scanNum_Panel  extends JPanel {
    public Check_in_scanNum_Panel(){
        //Set background color
        setBackground(new Color(72,46,115));
        //Set initial panel
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72,46,115));
        panel.setPreferredSize(new Dimension(1200,680));
        add(panel);

        JLabel label1 = new JLabel("PLEASE PUT YOUR ID DOCUMENT UNDER THE CAMERA TO SCAN");
        JLabel label2 = new JLabel("AFTER COMPLETING THE SCAN, CLICK CONFIRM");
        JLabel icon = new JLabel();
        int width = 600,height = 300;//This is the width and height of the picture and JLabel
        ImageIcon image = new ImageIcon(Constant.RESOURCE_PATH + "card.png");//Instantiate imageicon object
        image.setImage(image.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT ));
        icon.setIcon(image);

        label1.setFont(new Font(Font.DIALOG,Font.PLAIN,28));//Set text font
        label1.setForeground(Color.white);//Set text color
        label2.setFont(new Font(Font.DIALOG,Font.PLAIN,28));//Set text font
        label2.setForeground(Color.white);//Set text color

        panel.setLayout(null);

        label1.setBounds(150,50,1000,50);
        panel.add(label1);
        label2.setBounds(250,100,1000,50);
        panel.add(label2);
        icon.setBounds(300,-180,1000,1000);
        panel.add(icon);






    }
}
