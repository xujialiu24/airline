package frame.UserPanel;


import constant.Constant;
import frame.Main_Frame;
import model.eachFlight;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * <p>This page is mainly for passengers to confirm the previously selected information for final confirmation.</p>
 *
 * @author Qingwei Gao
 * @version 3.1
 */
public class Confirm_Panel extends JPanel {
    public Confirm_Panel() {

        //Set background color
        setBackground(new Color(72, 46, 115));
        //Set initial panel
        JPanel panel = new JPanel();
        panel.setBackground(new Color(72, 46, 115));
        panel.setPreferredSize(new Dimension(1200, 680));
        add(panel);

        JPanel panel1 = new JPanel();
        panel1.setBackground(new Color(96,62,151));

        Main_Frame.foodType=Foods_Panel.food;







        JLabel label1 = new JLabel("PLEASE PUT YOUR ");
        JLabel label2 = new JLabel("ID DOCUMENT");
        JLabel label3 = new JLabel("UNDER THE CAMERA TO SCAN");
        JLabel label4 = new JLabel("AFTER");
        JLabel label5 = new JLabel("CHECKING THE INFORMATION");
        JLabel label6 = new JLabel("AND");
        JLabel label7 = new JLabel("COMPLETING THE SCAN");
        JLabel label8 = new JLabel("CLICK");
        JLabel label9 = new JLabel("CONFIRM");
        JLabel label11 = new JLabel("BOARDING TIME");
        JLabel label12 = new JLabel("GATE");
        JLabel label13 = new JLabel("SEATS");
        JLabel label14 = new JLabel("FOODS");
        JLabel label15 = new JLabel(Main_Frame.flightNum);
        JLabel label16 = new JLabel(eachFlight.boardingTime(Main_Frame.flightNum));
        JLabel label17 = new JLabel(eachFlight.GATE(Main_Frame.flightNum));
        JLabel label18 = new JLabel(Seats_Panel.number+" "+Seats_Panel.letter);
        JLabel label19 = new JLabel(Main_Frame.foodType);
        JLabel label20 = new JLabel(eachFlight.COMPANY(Main_Frame.flightNum));



        label1.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//Set text font
        label1.setForeground(Color.white);//Set text color
        label3.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//Set text font
        label3.setForeground(Color.white);//Set text color
        label4.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//Set text font
        label4.setForeground(Color.white);//Set text color
        label6.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//Set text font
        label6.setForeground(Color.white);//Set text color
        label8.setFont(new Font(Font.DIALOG,Font.PLAIN,25));//Set text font
        label8.setForeground(Color.white);//Set text color
        label2.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label2.setForeground(Color.white);//Set text color
        label5.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label5.setForeground(Color.white);//Set text color
        label7.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label7.setForeground(Color.white);//Set text color
        label9.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label9.setForeground(Color.white);//Set text color
        label11.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label11.setForeground(Color.white);//Set text color
        label12.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label12.setForeground(Color.white);//Set text color
        label13.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label13.setForeground(Color.white);//Set text color
        label14.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label14.setForeground(Color.white);//Set text color
        label15.setFont(new Font(Font.DIALOG,Font.BOLD,26));//Set text font
        label15.setForeground(Color.white);//Set text color
        label16.setFont(new Font(Font.DIALOG,Font.PLAIN,26));//Set text font
        label16.setForeground(Color.white);//Set text color
        label17.setFont(new Font(Font.DIALOG,Font.PLAIN,26));//Set text font
        label17.setForeground(Color.white);//Set text color
        label18.setFont(new Font(Font.DIALOG,Font.PLAIN,26));//Set text font
        label18.setForeground(Color.white);//Set text color
        label19.setFont(new Font(Font.DIALOG,Font.PLAIN,26));//Set text font
        label19.setForeground(Color.white);//Set text color
        label20.setFont(new Font(Font.DIALOG,Font.PLAIN,26));//Set text font
        label20.setForeground(Color.white);//Set text color



        JLabel icon = new JLabel();
        int width = 600,height = 300;//This is the width and height of the picture and JLabel
        ImageIcon image = new ImageIcon(Constant.RESOURCE_PATH + "card.png");//Instantiate imageicon object
        image.setImage(image.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT ));
        icon.setIcon(image);


        panel.setLayout(null);
        icon.setBounds(300,-350,1000,1000);
        panel.add(icon);
        panel1.setBounds(100,300,1000,150);
        panel.add(panel1);


        JButton button = new JButton("<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SPECIAL<br>REQUIREMENT</html>");//add button
        button.setFont(new Font(Font.DIALOG,Font.PLAIN,20));//Set text font
        button.setForeground(Color.white);//Set text color
        button.setBackground(new Color(218,65,71));//Set background color

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main_Frame.req =JOptionPane.showInputDialog(null,"Please enter your special requirements");
            }
        });



        button.setBounds(950,50,170,80);
        panel.add(button);

        label1.setBounds(150,450,480,100);
        panel.add(label1);
        label2.setBounds(420,450,200,100);
        panel.add(label2);
        label3.setBounds(630,450,1000,100);
        panel.add(label3);
        label4.setBounds(120,500,1000,100);
        panel.add(label4);
        label5.setBounds(220,500,1000,100);
        panel.add(label5);
        label6.setBounds(660,500,1000,100);
        panel.add(label6);
        label7.setBounds(740,500,1000,100);
        panel.add(label7);
        label8.setBounds(490,550,1000,100);
        panel.add(label8);
        label9.setBounds(610,550,1000,100);
        panel.add(label9);

        panel1.setLayout(null);
        label11.setBounds(400,-20,1000,100);
        panel1.add(label11);
        label12.setBounds(800,-20,1000,100);
        panel1.add(label12);
        label13.setBounds(300,70,1000,100);
        panel1.add(label13);
        label14.setBounds(600,70,1000,100);
        panel1.add(label14);
        label15.setBounds(50,-20,1000,100);
        panel1.add(label15);
        label16.setBounds(650,-20,1000,100);
        panel1.add(label16);
        label17.setBounds(900,-20,1000,100);
        panel1.add(label17);
        label18.setBounds(400,70,1000,100);
        panel1.add(label18);
        label19.setBounds(730,70,1000,100);
        panel1.add(label19);
        label20.setBounds(180,-20,1000,100);
        panel1.add(label20);




    }
}