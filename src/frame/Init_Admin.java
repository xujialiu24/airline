package frame;
/**
 * Title             : Init_Admin.java
 * Description : This class is the startup class for launching the admin interface.
 */
public class Init_Admin {
    public static void main(String[] args) {
        new Admin_Frame().build_Admin_Frame();
        Admin_Frame.IDnum="";
        Admin_Frame.flag=false;
    }
}
